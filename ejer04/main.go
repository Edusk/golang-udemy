package main

import (
	"bufio"
	"fmt"
	"os"
)

var n1 int
var n2 int
var resultado int
var leyenda string

func main() {
	fmt.Println("Ingrese numero 1: ")
	fmt.Scanln(&n1)
	//fmt.Scanf("%d", &n1) Esto no en windows

	fmt.Println("Ingrese numero 2: ")
	fmt.Scanln(&n2)
	//fmt.Scanf("%d", &n2)

	fmt.Println("Descripcion:")

	scanner := bufio.NewScanner(os.Stdin) //Objeto scanner
	if scanner.Scan() {
		leyenda = scanner.Text()
	}

	resultado = n1 + n2
	fmt.Println(leyenda, resultado)
}
