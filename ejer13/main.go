package main

import "log"

func main() {
	/* archivo := "prueba.txt"
	f, err := os.Open(archivo)
	if err != nil {
		fmt.Println("Error abriendo el archivo")
		os.Exit(1)
	}

	defer f.Close() */

	ejemploPanic()
}

func ejemploPanic() {
	defer func() { //Si necesito mas de una instruccion, se usa una funcion anonima
		reco := recover()
		if reco != nil {
			log.Fatalf("ocurrio un error que genero un panic \n %v", reco)
		}
	}()
	a := 1
	if a == 1 {
		panic("Se encontro el valor de 1")
	}
}
