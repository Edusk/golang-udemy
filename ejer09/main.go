package main

import (
	"ejercicios/ejer09/user"
	"fmt"
	"time"
)

type pepe struct {
	user.Usuario //Herencia
}

func main() {
	us := new(pepe)
	us.NuevoUsuario(1, "Eduardo Campos", time.Now(), true)

	us2 := new(pepe)
	us2.NuevoUsuario(2, "Geovanni", time.Now(), true)

	fmt.Println("Usuario:", us)
	fmt.Println("Usuario:", us2)
}
