package main

import "fmt"

func main() {
	fmt.Println(uno(5))

	numero, estado := dos(1)
	fmt.Println(numero)
	fmt.Println(estado)

	fmt.Println(Calculo(5))
	fmt.Println(Calculo(5, 10, 45))
}

func uno(numero int) (z int) {
	z = numero * 2
	return
}

func dos(numero int) (int, bool) {
	if numero == 1 {
		return 5, false
	} else {
		return 10, true
	}
}

func Calculo(numero ...int) int {
	total := 0
	for _, v := range numero {
		total += v
	}
	return total
}
