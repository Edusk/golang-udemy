package main

import "fmt"

func main() {
	tabla := []int{4, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Println(tabla)

	variante2()
	variante3()
	variante4()

	/* for i := 0; i < len(tabla); i++ {
		fmt.Println(tabla[i])
	} */
}

func variante2() {
	elementos := [5]int{1, 2, 3, 4, 5}
	porcion := elementos[3:]

	fmt.Println(porcion)
}

func variante3() {
	elementos := make([]int, 5, 20)
	fmt.Printf("Largo %d, Capacidad %d", len(elementos), cap(elementos))
}

func variante4() {
	nums := make([]int, 0, 0)
	for i := 0; i < 100; i++ {
		nums = append(nums, i)
	}

	fmt.Printf("\nLargo %d, Capacidad %d", len(nums), cap(nums))
}
