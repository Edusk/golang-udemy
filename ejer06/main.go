package main

import "fmt"

var Calculo func(int, int) int = func(i1, i2 int) int {
	return i1 + i2
}

func main() {
	fmt.Printf("Sumo 5 + 7 = %d\n", Calculo(5, 7))

	//Restamos
	Calculo = func(i1, i2 int) int {
		return i1 - i2
	}

	fmt.Printf("Resto 10 - 7 = %d\n", Calculo(10, 7))

	//Dividimos
	Calculo = func(i1, i2 int) int {
		return i1 / i2
	}

	fmt.Printf("Dividir 12 / 3 = %d\n", Calculo(12, 3))

	Operaciones()

	// Closures
	tablaDel := 2
	MiTabla := Tabla(tablaDel)

	for i := 1; i <= 10; i++ {
		fmt.Println(MiTabla())
	}
}

func Operaciones() {
	resultado := func() int {
		a := 23
		b := 13
		return a + b
	}

	fmt.Println(resultado())
}

func Tabla(valor int) func() int {
	numero := valor
	secuencia := 0
	return func() int {
		secuencia += 1
		return numero * secuencia
	}
}
